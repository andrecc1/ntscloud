package com.cn.nts.spring.cloud.web.admin.feign.config;

import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class HystrixDashboardConfiguration {

    @Bean
    public ServletRegistrationBean getServlet() {
        HystrixMetricsStreamServlet streamServlet = new HystrixMetricsStreamServlet();
        ServletRegistrationBean res = new ServletRegistrationBean(streamServlet);

        res.setLoadOnStartup(1);
        res.addUrlMappings("/hystrix.stream");
        res.setName("HystrixMetricsStreamServlet");

        return res;
    }

}
