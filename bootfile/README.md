## URL地址和资源文件映射如下:

/{application}/{profile}[/{label}]
/{application}-{profile}.yml
/{label}/{application}-{profile}.yml
/{application}-{profile}.properties
/{label}/{application}-{profile}.properties

注意：	
第一个规则的分支名是可以省略的，默认是master分支
无论你的配置文件是properties，还是yml，只要是应用名+环境名能匹配到这个配置文件，那么就能取到
如果是想直接定位到没有写环境名的默认配置，那么就可以使用default去匹配没有环境名的配置文件
使用第一个规则会匹配到默认配置
如果直接使用应用名来匹配，会出现404错误，此时可以加上分支名匹配到默认配置文件
如果配置文件的命名很由多个-分隔，此时直接使用这个文件名去匹配的话，会出现直接将内容以源配置文件内容直接返回，内容前可能会有默认配置文件的内容
如果文件名含有多个“-”，则以最后一个“-”分割{application}和{profile}，若文件名为：my-app-demo-dev.properties，则映射的url为"/my-app-demo/dev"
示例：资源文件为myapp-dev.properties，对应url为:http://xxx/myapp/dev

客户端配置：
spring.application.name=xxxx
spring.cloud.config.profile=dev
spring.cloud.config.label=test
上述配置与资源文件对应关系为：
spring.application.name对应{application}
spring.cloud.config.profile对应{profile}
spring.cloud.config.label对应{label}

## java jar 启动方式
1. 先进到相应目录下执行 mvn clean package
2. 再到target目录下
3. java -jar nts-cloud-web-admin-feign-1.0.0.jar
4. 指定服务器端口启动 java -jar nts-cloud-web-admin-feign-1.0.0.jar --spring.profiles.active = prod