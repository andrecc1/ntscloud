package com.cn.nts.spring.cloud.service.admin.ribbon.controller;


import com.cn.nts.spring.cloud.service.admin.ribbon.service.AdminService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class AdminController {

    @Resource
    private AdminService adminService;

    @RequestMapping(value = "memo", method = RequestMethod.GET)
    public String sayHi(String msg) {
        return adminService.sayHi(msg);
    }

}
