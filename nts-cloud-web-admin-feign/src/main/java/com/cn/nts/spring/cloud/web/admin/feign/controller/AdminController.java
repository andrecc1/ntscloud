package com.cn.nts.spring.cloud.web.admin.feign.controller;


import com.cn.nts.spring.cloud.web.admin.feign.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController {

    @Autowired
    private AdminService adminService;

    @RequestMapping(value = "memo", method = RequestMethod.GET)
    public  String sayHi(String msg){
        return  adminService.sayHi(msg);
    }
}
