package com.cn.nts.spring.cloud.web.admin.feign.service;


import com.cn.nts.spring.cloud.web.admin.feign.service.hystrix.AdminServiceHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "nts-cloud-service-admin",fallback = AdminServiceHystrix.class)
public interface AdminService {
    @RequestMapping(value = "memo", method = RequestMethod.GET)
    public String sayHi(@RequestParam(value = "msg") String msg);
}
