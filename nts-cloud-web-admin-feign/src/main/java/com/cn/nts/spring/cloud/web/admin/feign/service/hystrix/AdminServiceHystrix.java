package com.cn.nts.spring.cloud.web.admin.feign.service.hystrix;

import com.cn.nts.spring.cloud.web.admin.feign.service.AdminService;
import org.springframework.stereotype.Component;

@Component
public class AdminServiceHystrix  implements AdminService {

    @Override
    public String sayHi(String msg) {
        return "hi your msg is :"+msg+"but request error.by Feign";
    }
}
