package com.cn.nts.spring.cloud.service.admin.ribbon.service;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@Service
public class AdminService {

    @Resource
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "hiError")
    public String sayHi(String msg){
        String tmp=restTemplate.getForObject("http://nts-cloud-service-admin/memo?msg="+msg,String.class);
        return tmp;
    }

    public String hiError(String msg){
        return "hi your msg is :"+msg+"but request error.";
    }

}
