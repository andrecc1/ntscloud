package com.cn.nts.spring.cloud.service.admin.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController {

    @Value("${server.port}")
    private String port;

    @RequestMapping(value = "memo",method = RequestMethod.GET)
    public String sendRestMemo(String msg ){
        String memo="RestMemo From and your Port is: "+port+"your msg :" +msg;
        return memo;
    }
}
